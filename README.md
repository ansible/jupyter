# JupyterLab and JupyterHub

This Ansible role installs JupyterLab behind JupyterHub
all inside a Python virtualenv.
Tested on Ubuntu server 22.04.

This is the actual code I use to maintain my own Jupyter server.
The code should be workable as-is for others, as long as you reset the
role variables and create your own cookie secret.

If you face problems, contact me directly or open an issue, and
I'd be happy to help you!


## Dependencies (other Ansible roles)

+ https://codeberg.org/ansible/python3
+ https://codeberg.org/ansible/nodejs
+ https://codeberg.org/ansible/julia (only if `jupyter_kernel_julia` is set to `true`)
+ https://codeberg.org/ansible/R (only if `jupyter_kernel_R` is set to `true`)

NOTE! The R role has dependencies of its own. If you want the R kernel,
the R role presently depends on TeXLive, which is a rather large download
and installation. I suggest you keep the R kernel disabled, unless you
are willing to either install TeXLive or edit the R role manually to remove
that dependency.


## Example playbook

This section is written with an Ansible beginner in mind.

This is a very rudimentary example. The web contains plenty of guides on
how to write Ansible playbooks.

In short, you will need to install Ansible, then create a directory
to hold your playbook and create the following files in it: `hosts.yml`,
`ansible.cfg`, and `playbook.yml`.
Here are some working examples to get you started (replace all
chevron-enclosed strings, `<>`, with your own):

```
### hosts.yml
all:
  vars:
    ansible_python_interpreter: /usr/bin/python3
<hosts-groupname>:
  hosts:
    <hostname>:
      ansible_host: <hostname>
```

`ansible_host` is where you tell Ansible how to reach your host.
It can be an IP address, but I suggest you learn to use SSH and `~/.ssh/config`.
Then you can just use the same `Host`-name in your Ansible hosts file,
and Ansible will obediently follow your ssh configuration.

It's also possible to install Jupyter etc. on the same machine where you
have installed Ansible (you will want to search for "running Ansible playbook
on localhost").

```
### ansible.cfg
### https://github.com/ansible/ansible/blob/devel/examples/ansible.cfg
[defaults]
inventory = ./hosts.yml
[privilege_escalation]
become_method = sudo
```

```
### playbook.yml
- hosts: <hosts-groupname>
  become: yes
  roles:
    - { role: jupyter }
```

Make sure you download this Jupyter role *and* all its dependency roles into the
folder `roles/` inside your playbook directory. I suggest you simply `git clone`
each role from inside your `roles/` directory.
You should end up with a tree looking something like this:

```
.
├── ansible.cfg
├── hosts.yml
├── playbook.yml
└── roles
    ├── jupyter
    ├── python3
    └── nodejs
```

Finally, before running the playbook, please generate a cookie secret for Jupyter
by entering the `./roles/jupyter/` directory and saving a random string to
the `files` subdirectory, like this:

```
cd ./roles/jupyter
sudo openssl rand -hex 32 > files/jupyterhub_cookie_secret
```

Now run your playbook (make sure you are in your playbook's top directory):

```
ansible-playbook playbook.yml --ask-become-pass
```

Since this role installs system packages, you will need to pass your `sudo`
password for the machine Ansible is targeting to the playbook.
This is what `--ask-become-pass` does--it makes Ansible ask for your password.
If you don't provide a password (or the wrong password), the playbook will fail
at the first task that requires elevated rights (but not before).


## Known issue

Upon successful login, we get stuck on a loading page saying
"Your server is starting up. You will be redirected automatically when it's ready for you",
but it never does.
Refreshing the page (F5) or clicking the "Home" link or navigating using the visible
JupyterLab UI takes us past this page, and then everything works normally.
Not sure why the redirect to `lab/` does not work (haven't really investigated this issue).


## Links and notes

### Jupyter and its subprojects

+ https://github.com/jupyterhub/jupyterhub
+ https://jupyterhub.readthedocs.io/en/latest/getting-started/config-basics.html
+ https://jupyterlab.readthedocs.io/en/stable/getting_started/installation.html
+ https://jupyterlab.readthedocs.io/en/stable/user/jupyterhub.html#jupyterhub
+ https://jupyterhub.readthedocs.io/en/stable/reference/config-user-env.html
+ https://blog.thebearsenal.com/2019/08/setting-up-jupyter-lab-integrated-with.html
+ https://blog.jupyter.org/please-dont-disable-authentication-in-jupyter-servers-dd197206e7f6?source=rss----95916e268740---4
+ https://jupyter.org/widgets


### Julia kernel

+ https://github.com/fredrikekre/jlpkg
+ https://docs.julialang.org/en/v1/manual/command-line-options/
+ https://github.com/JuliaLang/julia/issues/8485
+ https://pierreh.eu/upgrading-julia-and-ijulia-on-linux/
+ https://pkgdocs.julialang.org/v1/api/


### Other kernels

+ https://github.com/jenkstom/ansible-jupyterhub
